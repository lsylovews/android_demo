package com.example.newdict;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import Sqlite.MySqliteHelper;

public class MainActivity extends AppCompatActivity {

    private EditText edit_username,edit_psw;
    private MySqliteHelper sqliteHelper;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        sqliteHelper=new MySqliteHelper(this,"login.db3",null,1);
        db=sqliteHelper.getWritableDatabase();
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=edit_username.getText().toString();
                String psw=edit_psw.getText().toString();
                if(name.equals("")||psw.equals("")){
                    Toast.makeText(MainActivity.this,"用户名或密码为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                Cursor cursor=db.rawQuery("select * from user where name=? and password=?",new String[]{edit_username.getText().toString(),edit_psw.getText().toString()});
                if(cursor.moveToNext()){
                    Toast.makeText(MainActivity.this,"登陆成功",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this,"用户名或密码错误",Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=edit_username.getText().toString();
                String psw=edit_psw.getText().toString();
                if(name.equals("")||psw.equals("")){
                    Toast.makeText(MainActivity.this,"用户名或密码为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                Cursor cursor=db.rawQuery("select * from user where name=?",new String[]{edit_username.getText().toString()});
                if(cursor.moveToNext()){
                    Toast.makeText(MainActivity.this,"用户名已存在",Toast.LENGTH_SHORT).show();
                }else{
                    db.execSQL("insert into user(name,password) values(?,?)",new String[]{edit_username.getText().toString(),edit_psw.getText().toString()});
                    Toast.makeText(MainActivity.this,"注册成功",Toast.LENGTH_SHORT).show();
                }
                edit_psw.setText("");
                edit_username.setText("");
            }
        });
    }

    void initView(){
        edit_psw=findViewById(R.id.password);
        edit_username=findViewById(R.id.username);
    }
}
