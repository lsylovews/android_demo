package com.example.liushiyin.homework;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class CRActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout4);

        ((RadioGroup)findViewById(R.id.langue)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                String str=((RadioButton)findViewById(checkedId)).getText().toString();
                Toast.makeText(CRActivity.this,str,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        String str=((CheckBox)v).getText().toString();
        Toast.makeText(CRActivity.this,str,Toast.LENGTH_LONG).show();
    }
}
