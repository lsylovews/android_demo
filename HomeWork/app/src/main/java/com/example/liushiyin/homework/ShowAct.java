package com.example.liushiyin.homework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ShowAct extends Activity {

    TextView show_username;
    TextView show_psw;
    TextView show_agree;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        show_username=findViewById(R.id.show_username);
        show_psw=findViewById(R.id.show_psw);
        show_agree=findViewById(R.id.show_agrees);

        Intent intent=this.getIntent();
        show_username.setText("用户名:     "+intent.getStringExtra("username"));
        show_psw.setText("密码:     "+intent.getStringExtra("psw"));
        show_agree.setText("同意条款:"+intent.getStringExtra("agree"));
    }
}
