package com.example.autologin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText edit_username,edit_password;
    private CheckBox ch_svae_psw,ch_auto_login;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=edit_username.getText().toString();
                String password=edit_password.getText().toString();
                if(ch_svae_psw.isChecked()){
                    editor.putString("password",password);
                    editor.putString("username",username);
                }
                editor.putBoolean("isSavepsw",ch_svae_psw.isChecked());
                editor.putBoolean("isAuto",ch_auto_login.isChecked());
                editor.commit();
                Intent intent=new Intent(MainActivity.this,ShowActivity.class);
                startActivity(intent);
            }
        });
        ch_svae_psw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("isSavepsw",isChecked);
                editor.commit();
            }
        });
        ch_auto_login.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("isAuto",isChecked);
                editor.commit();
            }
        });
        readConf();
    }

    void initView(){
        edit_username=findViewById(R.id.username);
        edit_password=findViewById(R.id.password);
        ch_svae_psw=findViewById(R.id.ch_save_psw);
        ch_auto_login=findViewById(R.id.ch_auto_login);
    }

    void readConf(){
        sharedPreferences=getSharedPreferences("autologin",MODE_PRIVATE);
        editor=sharedPreferences.edit();
        String name=sharedPreferences.getString("username",null);
        String psw=sharedPreferences.getString("password",null);
        Boolean isAuto=sharedPreferences.getBoolean("isAuto",false);
        Boolean isSavepsw=sharedPreferences.getBoolean("isSavepsw",false);
        ch_auto_login.setChecked(isAuto);
        ch_svae_psw.setChecked(isSavepsw);
        if(isAuto){
            if(name!=null&&psw!=null){
                edit_username.setText(name);
                edit_password.setText(psw);
                Intent intent=new Intent(MainActivity.this,ShowActivity.class);
                startActivity(intent);
            }
        }else if(isSavepsw){
            if(name!=null)
                edit_username.setText(name);
            if(psw!=null)
                edit_password.setText(psw);
        }
    }
}
