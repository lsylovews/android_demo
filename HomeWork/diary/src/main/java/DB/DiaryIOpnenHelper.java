package DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.DiscretePathEffect;

public class DiaryIOpnenHelper extends SQLiteOpenHelper{

    final String CREATE_TABLE="create table diary(id integer primary key autoincrement,topic char(100),content char(1000))";

    public DiaryIOpnenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists diary");
        db.execSQL(CREATE_TABLE);
    }
}
