package com.example.diary;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import DB.DiaryIOpnenHelper;

public class MainActivity extends AppCompatActivity {

    DiaryIOpnenHelper helper;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helper=new DiaryIOpnenHelper(this,"diaryOpenHelper.db",null,1);
        db=helper.getWritableDatabase();
    }
}
