package Model;

import java.io.Serializable;

public class ItemInfo implements Serializable {

    private String name;
    private int life;
    private int attack;
    private int speed;

    public ItemInfo() {
        this.name = "";
        this.life = 0;
        this.attack = 0;
        this.speed = 0;
    }

    public ItemInfo(String name, int life, int attack, int speed) {
        this.name = name;
        this.life = life;
        this.attack = attack;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String toString(){
        return "[name="+name+"attack="+attack+"speed="+speed+"]";
    }
}
