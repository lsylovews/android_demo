package com.example.equipmentshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import Model.ItemInfo;

public class MainActivity extends AppCompatActivity {

    ProgressBar hp_bar,atk_bar,sp_bar;
    TextView hp_text,atk_text,sp_text;
    final static int RESULT_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        findViewById(R.id.id_buy_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ShopActivity.class);
                startActivityForResult(intent,RESULT_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data!=null){
            if(resultCode==RESULT_CODE){
                ItemInfo itemInfo=(ItemInfo) data.getSerializableExtra("equip");
                updateProgress(itemInfo);
            }
        }
    }

    void initView() {
        hp_bar=findViewById(R.id.id_life);
        atk_bar=findViewById(R.id.id_atk);
        sp_bar=findViewById(R.id.id_speed);
        hp_text=findViewById(R.id.id_life_value);
        atk_text=findViewById(R.id.id_atk_value);
        sp_text=findViewById(R.id.id_sp_value);
        hp_bar.setProgress(20);
        atk_bar.setProgress(20);
        sp_bar.setProgress(20);
    }

    void updateProgress(ItemInfo itemInfo){
        hp_bar.setProgress(hp_bar.getProgress()+itemInfo.getLife());
        atk_bar.setProgress(atk_bar.getProgress()+itemInfo.getAttack());
        sp_bar.setProgress(sp_bar.getProgress()+itemInfo.getSpeed());
        hp_text.setText(hp_bar.getProgress()+"");
        atk_text.setText(atk_bar.getProgress()+"");
        sp_text.setText(sp_bar.getProgress()+"");
    }
}
