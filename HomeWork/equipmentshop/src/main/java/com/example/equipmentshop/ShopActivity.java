package com.example.equipmentshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import Model.ItemInfo;

public class ShopActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private ItemInfo itemInfo=new ItemInfo();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        radioGroup=findViewById(R.id.id_radio);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.id_item1:
                        itemInfo=new ItemInfo("item1",60,30,10);
                        break;
                    case R.id.id_item2:
                        itemInfo=new ItemInfo("item2",10,50,10);
                        break;
                    case R.id.id_item3:
                        itemInfo=new ItemInfo("item3",10,30,60);
                        break;
                    default:
                        break;
                }
            }
        });

        findViewById(R.id.id_buy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("equip",itemInfo);
                setResult(MainActivity.RESULT_CODE,intent);
                finish();
            }
        });
    }
}
