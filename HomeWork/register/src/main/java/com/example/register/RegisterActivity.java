package com.example.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    private CheckBox box1,box2,box3,box4;
    private RadioGroup radioGroup;
    private TextView username;
    private String sex="男";
    private String love="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        findViewById(R.id.id_btn_reg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                love="";
               love+=box1.isChecked()?box1.getText()+" ":"";
               love+=box2.isChecked()?box2.getText()+" ":"";
               love+=box3.isChecked()?box3.getText()+" ":"";
               love+=box4.isChecked()?box4.getText()+" ":"";
               Intent intent=new Intent(RegisterActivity.this,ShowActivity.class);
               intent.putExtra("name",username.getText().toString());
               intent.putExtra("sex",sex);
               intent.putExtra("love",love);
               startActivity(intent);
            }
        });

        radioGroup=findViewById(R.id.id_radio);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.id_boy:
                        sex="男";
                        break;
                    case R.id.id_girl:
                        sex="女";
                        break;
                }
            }
        });
    }

    void initView(){
        box1=findViewById(R.id.box_game);
        box2=findViewById(R.id.box_music);
        box3=findViewById(R.id.box_run);
        box4=findViewById(R.id.box_tv);
        username=findViewById(R.id.user_name);
    }
}
