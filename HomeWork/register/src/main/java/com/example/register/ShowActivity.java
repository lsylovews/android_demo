package com.example.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowActivity extends AppCompatActivity {

    private TextView username,sex,love;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        initView();
    }

    void initView(){
        username=findViewById(R.id.user_name);
        sex=findViewById(R.id.sex);
        love=findViewById(R.id.love);
        Intent intent=getIntent();
        username.setText(intent.getStringExtra("name"));
        sex.setText(intent.getStringExtra("sex"));
        love.setText(intent.getStringExtra("love"));
    }
}
