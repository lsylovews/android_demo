package com.example.sdsave;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.Buffer;

public class SdActivity extends AppCompatActivity {

    private EditText edit_write,edit_read;
    private CheckBox ch_append;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        findViewById(R.id.btn_write).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write(edit_write.getText().toString());
            }
        });

        findViewById(R.id.btn_read).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_read.setText(read());
            }
        });
    }

    void initView(){
        edit_read=findViewById(R.id.edit_read);
        edit_write=findViewById(R.id.edit_write);
        ch_append=findViewById(R.id.ch_append);
    }

    void write(String content){
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            File sdDir=Environment.getExternalStorageDirectory();
            try {
                File targetFile=new File(sdDir.getCanonicalPath()+File.separator+"file.txt");
                RandomAccessFile randomAccessFile=new RandomAccessFile(targetFile,"rw");
                if(ch_append.isChecked()){
                    randomAccessFile.seek(targetFile.length());
                }else{
                    randomAccessFile.seek(0);

                }
                randomAccessFile.write(content.getBytes());
                randomAccessFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    String read(){
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            File sdDir=Environment.getExternalStorageDirectory();
            try {
                File targetFile=new File(sdDir.getCanonicalPath()+File.separator+"file.txt");
                FileInputStream fileInputStream=new FileInputStream(targetFile);
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(fileInputStream));
                StringBuffer buffer=new StringBuffer();
                String line="";
                while ((line=bufferedReader.readLine())!=null){
                    buffer.append(line);
                }
                return buffer.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
       return "";
    }
}
